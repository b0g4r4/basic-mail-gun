//require('dotenv').config();

const nodemailer = require('nodemailer');
const mailGun = require('nodemailer-mailgun-transport');
const log = console.log;

// Step 1
const auth = {
    auth: {
        api_key: '0431a0c28661e963172e0cd7defada93-d32d817f-6bd8fb3e',
        domain: 'sandbox6a25a2306f7e4612b49f2af1ee475739.mailgun.org'
    }
};

// Step 2
let transporter = nodemailer.createTransport( mailGun(auth) );


// Step 3
let mailOptions = {
    from: 'b0g4r4@gmail.com',
    to: 'b0g4r4@gmail.com',
    subject: 'Nodemailer - Test',
    text: 'Wooohooo it works!!'
};

// Step 4
transporter.sendMail(mailOptions, (err, data) => {
    if (err) {
        return log('Error occurs');
    }
    return log('Email sent!!!');
});